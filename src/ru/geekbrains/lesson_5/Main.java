package ru.geekbrains.lesson_5;

import ru.geekbrains.lesson_5.employees.Employee;

public class Main {

    /**
     * Задание
     *
     * Создать класс "Сотрудник" с полями: ФИО, должность, email, телефон, зарплата, возраст.
     * Конструктор класса должен заполнять эти поля при создании объекта.
     * Внутри класса «Сотрудник» написать метод, который выводит информацию об объекте в консоль.
     * Создать массив из 5 сотрудников. С помощью цикла вывести информацию только о сотрудниках старше 40 лет.
     *
     * @param args
     */
    public static void main(String[] args) {

        Employee[] employees = {
            new Employee("Иванов Генадий Федорович", "Слесарь", "box1@mail.loc", "9876543210", 30000, 33),
            new Employee("Петров Петр Владимирович", "Бухгалтер", "box2@mail.loc", "9876543211", 30000, 24),
            new Employee("Сидоров Иван Иванович", "Электрик", "box3@mail.loc", "9876543212", 30000, 32),
            new Employee("Николаев Павел Степанович", "Дворник", "box4@mail.loc", "9876543213", 30000, 55),
            new Employee("Комаров Иван Николаевич", "Грузчик", "box5@mail.loc", "9876543214", 30000, 45),
        };

        for (int i = 0; i < employees.length; i++) {
            if (employees[i].isOlder(40)) employees[i].printInfo();
        }
    }
}