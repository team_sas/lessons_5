package ru.geekbrains.lesson_5.employees;

public class Employee {

    private String name;
    private String occupation;
    private String email;
    private String phone;
    private int salary;
    private int age;

    public Employee(String name, String occupation, String email, String phone, int salary, int age) {
        this.name = name;
        this.occupation = occupation;
        this.email = email;
        this.phone = phone;
        this.salary = salary;
        this.age = age;
    }

    public void printInfo() {
        System.out.println("---");
        System.out.println("ФИО: " + this.name);
        System.out.println("Должость: " + this.occupation);
        System.out.println("Email: " + this.email);
        System.out.println("Телефон: " + this.phone);
        System.out.println("Зарплата: " + this.salary);
        System.out.println("Возраст: " + this.age);
        System.out.println("---");
    }

    public boolean isOlder(int age) {
        return this.age > age;
    }
}
